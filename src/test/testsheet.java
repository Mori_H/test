package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class testsheet {

	public static void main(String[] args) {


		BufferedReader br = null;  //ｂｒにnull

		try {
			// 1.ファイルの場所、名前を指定
			File file = new File(args[0],"branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr); //生成したｆｒ情報入りBRをｂｒに。


			// shopデータを格納するリスト
			//            List<String>shopdata = new ArrayList<>();
			//1行で読み込み、nullじゃないときに
			TreeMap<String,String>map = new TreeMap<>();
			Map<String,Long>shopTotal= new TreeMap<>();

			String line;

			while ((line = br.readLine()) != null) {
				// コンマで行を分割
				String[] shopCode = line.split(",");

				//支店コードと支店名を結び付けたい
				map.put(shopCode[0],shopCode[1]);  //キーと文字列の結びつき
				System.out.println(shopCode[0] + "," + shopCode[1]);


			if( (shopCode[0]).matches("^[0-9]{3}") != true || shopCode[1].endsWith("支店") != true) {
				System.out.println("支店定義ファイルのフォーマットが不正です。");
				br.close();
				System.exit(0);
			}
			}

			List<String> codeList = new ArrayList<String>(map.keySet());
			for(String codelist:codeList) {
				shopTotal.put(codelist, (long)0);//下にも。下書き的な。
			}



			/* フィルタ作成 */
			FilenameFilter filter = new FilenameFilter() {
				/* ここに条件を書く。trueの場合、そのファイルは選択される */
				public boolean accept(File file, String str) {

					//指定文字列でフィルタする
					if(str.matches("[0-9]{8}" + ".rcd")) {
						return true;
					} else {
						return false;
					}
				}
			};

			//listfilesメソッドを使用して一覧を取得する
			File[] list = new File(args[0]).listFiles(filter);


			if(list != null) {

				for(int i=0; i<list.length;i++) { //リスト内の要素数分ループ

//					DecimalFormat dformat = new DecimalFormat("00000000");
//					int n = i+1;
//					String dfn = dformat.format(n).toString();
//						if(list[i].getName().contains(dfn) != true) {
//						System.out.println("売上ファイル名が連番になっていません。");
//						br.close();
//						System.exit(0);
//						}


					if(list[i].isFile()) {
						System.out.println("読み込むファイルです : " + list[i].toString()); //リスト内の全リスト名を出力
					}else{
						System.out.println("null");
					}
				}


				//2-1完了
				//2-2


				BufferedReader br2 = null;
				try {
					// 1.ファイルの場所、名前を指定
					int i = 0;//追加
					for( i = 0;i<list.length;i++) { //フィルタしたファイル分繰り返す この数だけ いらん？


						if(list[i].isFile()) { //もしファイルだった場合

							File file2 =  (list[i]);
							FileReader fr2 = new FileReader(file2);
							br2 = new BufferedReader(fr2); //生成したｆｒ2情報入りBRをｂｒに。

							String x = br2.readLine();//1行目読む
							String y = br2.readLine();//2行目読む

							int lineCount = 0;
							String z;
							while ((z = br2.readLine()) != null) {
								lineCount++; //繰り返しごとにカウントアップ
							}
							if(lineCount != 0) {
								System.out.println(x + "のフォーマットが不正です");
								br2.close();
								System.exit(0);
							}

							Map<String,String>map2= new TreeMap<>();
							map2.put(x,y);  //キーと文字列の結びつき
							System.out.println(x + "の売上金額は" + y);//１ファイル目だとx001とy301000円

							Long t = Long.parseLong(y);//y(売上金額)はlongに。

							if(map.containsKey(x) == true) {//キーとｘが合ったら
								shopTotal.put(x, shopTotal.get(x)+ t);//ショップトータルに支店コード（キー）と単一売上金額
								//ループ処理、売上金額が積み重なるようにしていく。map.get(key)でvalueが出る。

								if((shopTotal.get(x)) > 1000000000) {
									System.out.println("合計金額が10桁を超えました");
									br2.close();
									System.exit(0);
								}
							}else {
								System.out.println(x + "の支店コードが不正です");
								br2.close();
								System.exit(0);
							}


						}
					}//ｆor文

				}//try文

				//エラー発生
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");

				}finally {  //最後にクローズ
					if(br2 != null) {  //もしｂｒに参照が入ってない状態じゃないならば
						try {
							br2.close();
						}catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
						}
					}
				}


				//									}
				//									catch(IOException e) {
				//										System.out.println("例外が発生しました。");
				//									}
				//								}//if文の支店コードがイコールだったら






			}

			File newfile = new File(args[0],"branch.out");
			if(!newfile.exists()) {//もしファイルがなかったら

				if(newfile.createNewFile()){
					System.out.println("ファイル作成成功");
				}else {
					System.out.println("ファイル作成失敗");
				}
			}//ファイルがあったら
			else {
				System.out.println("ファイルが存在しています。");
			}


			// FileWriterクラスを使用する
			FileWriter fw = new FileWriter(newfile);
			//PrintWriterクラスを使用する
			PrintWriter pw = new PrintWriter(new BufferedWriter(fw));


			// Iterator<Map.Entry<String, Integer>> の宣言
			Iterator<Map.Entry<String, String>> itr = map.entrySet().iterator();
			Iterator<Long> val_itr = shopTotal.values().iterator();
			// key, valueの取得
			while(itr.hasNext() && val_itr.hasNext()) {
				// nextを使用して値を取得する
				Map.Entry<String, String> e = itr.next();
				Long l = (Long)val_itr.next();

				pw.println(e.getKey() + " , " + e.getValue() + " , " + l );


			}

			//		        }


			//ファイルを閉じる
			pw.close();
			//				}


		}//でかtry



		catch(FileNotFoundException e){					//ファイルが存在しない場合（支店）
			System.out.println("支店定義のファイルが存在しません。");
		}
		//エラー発生
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");


		}finally {  //最後にクローズ
			if(br != null) {  //もしｂｒに参照が入ってない状態じゃないならば
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}



			}


		}
	}
}

